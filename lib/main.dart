import 'package:flutter/material.dart';
import './pages/Quest.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TopQuest',
      theme: ThemeData(
        primaryColor: Colors.white,
        fontFamily: 'Nunito Sans',
      ),
      home: Quest(),
    );
  }
}

