import 'package:flutter/material.dart';
import '../utils/CardData.dart';
import '../components/quest_card.dart';

class Quest extends StatelessWidget {
  final List<Widget> tabs = [
    Tab(text: 'Sedang Aktif'),
    Tab(text: 'Berjalan'),
    Tab(text: 'Selesai'),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              onPressed: null,
              icon: Icon(
                Icons.arrow_back,
                color: Colors.black54,
              ),
            ),
            title: Text(
              'TopQuest',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                color: Colors.black87,
              ),
            ),
            bottom: TabBar(
              indicatorColor: Colors.green,
              labelStyle: TextStyle(
                fontFamily: 'Nunito Sans',
                fontWeight: FontWeight.bold,
                color: Colors.red,
              ),
              labelColor: Colors.green,
              tabs: tabs,
            ),
          ),
          body: TabBarView(children: [
            SedangAktif(),
            Center(child: Text('Kosong')),
            Center(child: Text('Kosong')),
          ]),
        ));
  }
}

class SedangAktif extends StatelessWidget {
  final List cardData = [
    CardData(
      title: 'TopQuest',
      subtitle: 'Hadiah 500 OVO Points',
      content: 'Check-in 3 Hari Berturut-turut',
      timeRemaining: 5,
      gradientColors: [0xFFF474AB, 0xFF4A019E],
    ),
    CardData(
      title: 'Official Store',
      subtitle: 'Hadiah Kupon Cashback 100% + 8.000 OVO Points',
      content: 'Belanja 3x di Official Store Apa Saja!',
      timeRemaining: 2,
      gradientColors: [0xFF7B00C4, 0xFFD346AA],
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: ListView.builder(
        padding: EdgeInsets.all(15),
        itemCount: cardData.length,
        itemBuilder: (BuildContext context, int index) {
          print(cardData);
          return questCard(cardData[index]);
        },
      ),
    );
  }
}
