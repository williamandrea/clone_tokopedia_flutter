class CardData {
  String? title;
  String? subtitle;
  String? content;
  int? timeRemaining;
  List? gradientColors;

  CardData({
    required this.title,
    required this.subtitle,
    required this.content,
    required this.timeRemaining,
    required this.gradientColors,
  });
}
